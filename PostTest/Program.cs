﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using TSHRLib;

namespace PostTest
{
    class Program
    {
        static void Main(string[] args)
        {



            //string postUrl = "http://hrap.taishinbank.com.tw/api/notify/PushNotifications";
            string postUrl = "http://localhost:6658/notify/PushNotifications";
            //string str = "70f9280a3cbcdd1f14530ecb153e8b8aad64c3549e81b7fe8e452a665cd931ec";

            //var d = Security.Symmetric.AES256DecryptStrFromBytes(Data.Exchange.StringToByteArray(str));
            //Console.WriteLine(d);
            Console.WriteLine("輸入TOKEN : ");
            string tkn;
            tkn = Convert.ToString(Console.ReadLine());
            Console.WriteLine("\r\n");
            string postData = "{\"PUSH_TOKEN\":\"tkn\",\"PUSH_OBJECT\":[{\"TITLE\":\"林献群所申請的請假申請已簽核完成\",\"USER_ID\":\"1000200\",\"MESSAGE\":\"林献群所申請的請假申請已簽核完成\",\"DATE_TIME\":\"2017 / 12 / 06 00:00:00\"}],\"PUSH_TYPE\":2,\"PUSH_SYSCODE\":\"MHR\"}";
            postData=postData.Replace("tkn",tkn);

            Console.WriteLine($"= Post Data ============");
            Console.WriteLine(postData);
            Console.WriteLine("\r\n");
            using (var Client = new HttpClient())
            {
                //var Content = new StringContent(JsonConvert.SerializeObject(postData), Encoding.UTF8, "application/json");
                var Content = new StringContent(postData, Encoding.UTF8, "application/json");
                var Result = Client.PostAsync(postUrl , Content).Result;
                //  var item = Result.Content;
                //foreach (var item in loc_udtItems)
                //{
                //    item.CmpSerilNo = PwdHelper.AES256Decyrpt(item.CmpSerilNo);
                //    item.PerserilNo = PwdHelper.AES256Decyrpt(item.PerserilNo);
                //    item.UserAccount = PwdHelper.AES256Decyrpt(item.UserAccount);
                //    //item.CmpSerilNo = item.CmpSerilNo;
                //    //item.PerserilNo = item.PerserilNo;
                //    //item.UserAccount = item.UserAccount;
                //}
                var contentStr = Result.Content.ReadAsStringAsync();

                Console.WriteLine($"= HTTP Response ============");
                Console.WriteLine(Result);
                Console.WriteLine("\r\n");
                Console.WriteLine($"= Response Content ============");
                Console.WriteLine(contentStr.Result);
                Console.WriteLine("\r\n");
            }

            /*
             here for develope 
             */

            
            Console.ReadLine();
        }
    }
}
